<?php
/**
 * @file
 * uw_cfg_conference.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_cfg_conference_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function uw_cfg_conference_image_default_styles() {
  $styles = array();

  // Exported image style: conference_advertisement.
  $styles['conference_advertisement'] = array(
    'label' => 'Conference Advertisement',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 370,
          'height' => 614,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: conference_banner.
  $styles['conference_banner'] = array(
    'label' => 'Conference Banner',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1800,
          'height' => 768,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: conference_speaker.
  $styles['conference_speaker'] = array(
    'label' => 'Conference Speaker',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 480,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: conference_sponsor.
  $styles['conference_sponsor'] = array(
    'label' => 'Conference Sponsor',
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 520,
          'height' => 520,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_cfg_conference_node_info() {
  $items = array(
    'conference_advertisement' => array(
      'name' => t('Conference advertisement'),
      'base' => 'node_content',
      'description' => t('An advertisement for a conference website.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'conference_links' => array(
      'name' => t('Conference links'),
      'base' => 'node_content',
      'description' => t('A list of links to display on the homepage of a conference site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'conference_session' => array(
      'name' => t('Conference session'),
      'base' => 'node_content',
      'description' => t('A session for a conference website.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'conference_speaker' => array(
      'name' => t('Conference speaker'),
      'base' => 'node_content',
      'description' => t('A speaker for a conference website.'),
      'has_title' => '1',
      'title_label' => t('Full name for search'),
      'help' => '',
    ),
    'conference_sponsor' => array(
      'name' => t('Conference sponsor'),
      'base' => 'node_content',
      'description' => t('A sponsor for a conference website.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'conference_video' => array(
      'name' => t('Conference video'),
      'base' => 'node_content',
      'description' => t('A video for a conference website.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
