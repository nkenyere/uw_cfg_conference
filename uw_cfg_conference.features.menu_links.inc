<?php
/**
 * @file
 * uw_cfg_conference.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_cfg_conference_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_conference-settings:admin/settings/uw_conference
  $menu_links['menu-site-management_conference-settings:admin/settings/uw_conference'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/settings/uw_conference',
    'router_path' => 'admin',
    'link_title' => 'Conference Settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_conference-settings:admin/settings/uw_conference',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_homepage-settings:admin/settings/uw_conference_homepage
  $menu_links['menu-site-management_homepage-settings:admin/settings/uw_conference_homepage'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/settings/uw_conference_homepage',
    'router_path' => 'admin',
    'link_title' => 'Homepage Settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_homepage-settings:admin/settings/uw_conference_homepage',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_conference-blog-categories:admin/structure/taxonomy/conference_blog_categories
  $menu_links['menu-site-manager-vocabularies_conference-blog-categories:admin/structure/taxonomy/conference_blog_categories'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/conference_blog_categories',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Conference Blog Categories',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_conference-blog-categories:admin/structure/taxonomy/conference_blog_categories',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_conference-session-topics:admin/structure/taxonomy/conference_session_topics
  $menu_links['menu-site-manager-vocabularies_conference-session-topics:admin/structure/taxonomy/conference_session_topics'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/conference_session_topics',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Conference Session Topics',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_conference-session-topics:admin/structure/taxonomy/conference_session_topics',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_conference-session-types:admin/structure/taxonomy/conference_session_types
  $menu_links['menu-site-manager-vocabularies_conference-session-types:admin/structure/taxonomy/conference_session_types'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/conference_session_types',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Conference Session Types',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_conference-session-types:admin/structure/taxonomy/conference_session_types',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_conference-speaker-groups:admin/structure/taxonomy/conference_speaker_groups
  $menu_links['menu-site-manager-vocabularies_conference-speaker-groups:admin/structure/taxonomy/conference_speaker_groups'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/conference_speaker_groups',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Conference Speaker Groups',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_conference-speaker-groups:admin/structure/taxonomy/conference_speaker_groups',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_conference-sponsorship-levels:admin/structure/taxonomy/conference_sponsorship_levels
  $menu_links['menu-site-manager-vocabularies_conference-sponsorship-levels:admin/structure/taxonomy/conference_sponsorship_levels'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/conference_sponsorship_levels',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Conference Sponsorship Levels',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_conference-sponsorship-levels:admin/structure/taxonomy/conference_sponsorship_levels',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Conference Blog Categories');
  t('Conference Session Topics');
  t('Conference Session Types');
  t('Conference Settings');
  t('Conference Speaker Groups');
  t('Conference Sponsorship Levels');
  t('Homepage Settings');


  return $menu_links;
}
